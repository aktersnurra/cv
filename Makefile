.POSIX:

FILENAME = rydholm-gustaf-cv
all: clean compile

clean:
	rm -f *.aux *.log *.out *.pdf

compile:
	pdflatex cv.tex
	mv cv.pdf $(FILENAME).pdf

.PHONY: clean compile
